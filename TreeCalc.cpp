// Meghan O'Hara mmo2bf 10/9/16 TreeCalc.cpp
// TreeCalc.cpp:  CS 2150 Tree Calculator method implementations

#include "TreeCalc.h"
#include <iostream>
#include <stack>
#include <cstdlib>

using namespace std;

//Constructor
TreeCalc::TreeCalc() {
}

//Destructor- frees memory
TreeCalc::~TreeCalc() {
  while (!(mystack.empty())) {
	mystack.pop();
  }
  cleanTree(mystack.top()); 
}

//Deletes tree/frees memory
void TreeCalc::cleanTree(TreeNode* ptr) {
  if (ptr->left != NULL) {
    cleanTree(ptr->left);
  }
  else if (ptr->right != NULL) {
    cleanTree(ptr->right);
  }
  else if (ptr->left == NULL) {
    delete ptr->left;
  }
  else if (ptr->right == NULL) {
    delete ptr->right;
  }
  if (ptr->right == NULL && ptr->left == NULL) {
    delete ptr;
  }
}

// DO NOT ALTER !!
//Gets data from user
void TreeCalc::readInput() {
    string response;
    cout << "Enter elements one by one in postfix notation" << endl
         << "Any non-numeric or non-operator character,"
         << " e.g. #, will terminate input" << endl;
    cout << "Enter first element: ";
    cin >> response;
    //while input is legal
    while (isdigit(response[0]) || response[0]=='/' || response[0]=='*'
            || response[0]=='-' || response[0]=='+' ) {
        insert(response);
        cout << "Enter next element: ";
        cin >> response;
    }
}

//Puts value in tree stack
void TreeCalc::insert(const string& val) {
    // insert a value into the tree
  
  TreeNode *nodeVal = new TreeNode(val);
  
  if (val=="/" || val=="*" || val=="-" || val=="+" ) {
    TreeNode* rightNode = mystack.top();
      mystack.pop();
      TreeNode* leftNode = mystack.top();
      mystack.pop();
      nodeVal->left = leftNode;
      nodeVal -> right = rightNode;
      mystack.push(nodeVal); 
  }
    
  else {
    mystack.push(nodeVal); 
  }
    
}

//Prints data in prefix form
void TreeCalc::printPrefix(TreeNode* ptr) const {
    // print the tree in prefix format
  /* 
Algorithm prefix (tree)
Print the prefix expression for an expression tree.
 Pre : tree is a pointer to an expression tree
 Post: the prefix expression has been printed
 if (tree not empty)
    print (tree token)
    prefix (tree left subtree)
    prefix (tree right subtree)
 end if
end prefix
   */

  if (ptr != NULL) {
    cout << ptr->value << " ";
    printPrefix(ptr->left);
    printPrefix(ptr->right);
  }
  
}

//Prints data in infix form
void TreeCalc::printInfix(TreeNode* ptr) const {
    // print tree in infix format with appropriate parentheses
  /*
    Algorithm infix (tree)
    Print the infix expression for an expression tree.
    Pre : tree is a pointer to an expression tree
    Post: the infix expression has been printed
    if (tree not empty)
      if (tree token is operator)
        print (open parenthesis)
      end if
      infix (tree left subtree)
      print (tree token)
      infix (tree right subtree)
      if (tree token is operator)
        print (close parenthesis)
      end if
      end if
    end infix
   */

  if (ptr != NULL) {
    if (ptr->value=="/" || ptr->value=="*"|| ptr->value=="-" || ptr->value=="+" ){
      cout << "(";
    }
    printInfix(ptr->left);
    if (ptr->value=="/" || ptr->value=="*"|| ptr->value=="-" || ptr->value=="+" ){
      cout << " " << ptr->value << " ";
    }
    else {
      cout << ptr->value;
    }
    printInfix(ptr->right);
    if (ptr->value=="/" || ptr->value=="*"|| ptr->value=="-" || ptr->value=="+" ){
      cout << ")";
    }
  } 
}

//Prints data in postfix form
void TreeCalc::printPostfix(TreeNode* ptr) const {
    // print the tree in postfix form

  /*Algorithm postfix (tree)
Print the postfix expression for an expression tree.
 Pre : tree is a pointer to an expression tree
 Post: the postfix expression has been printed
 if (tree not empty)
    postfix (tree left subtree)
    postfix (tree right subtree)
    print (tree token)
 end if
end postfix
      */

  if (ptr != NULL) {
    printPostfix(ptr->left);
    printPostfix(ptr->right);
    cout << ptr->value << " ";
  }
  
}

// Prints tree in all 3 (pre,in,post) forms

void TreeCalc::printOutput() const {
    if (mystack.size()!=0 && mystack.top()!=NULL) {
        cout << "Expression tree in postfix expression: ";
        // call your implementation of printPostfix()
	printPostfix(mystack.top()); 
        cout << endl;
        cout << "Expression tree in infix expression: ";
        // call your implementation of printInfix()
	printInfix(mystack.top()); 
        cout << endl;
        cout << "Expression tree in prefix expression: ";
        // call your implementation of printPrefix()
	printPrefix(mystack.top()); 
        cout << endl;
    } else
        cout<< "Size is 0." << endl;
}

//Evaluates tree, returns value
// private calculate() method
int TreeCalc::calculate(TreeNode* ptr) const {
    // Traverse the tree and calculates the result

  int retVal= 0; 

  if (ptr != NULL) {
    if (ptr->left == NULL && ptr->right == NULL) {
      string str = ptr->value;
      retVal = atoi(str.c_str());
    }
    else if (ptr->value == "+") {
      int right = calculate(ptr->right);
      int left = calculate(ptr->left);
      retVal = (right + left); 
    }
    else if (ptr->value == "-") {
      int right = calculate(ptr->right);
      int left = calculate(ptr->left);
      retVal = (right - left); 
    }
    else if (ptr->value == "/") {
      int right = calculate(ptr->right);
      int left = calculate(ptr->left);
      retVal = (right / left); 
    }
    else if (ptr->value == "*") {
      int right = calculate(ptr->right);
      int left = calculate(ptr->left);
      retVal = (right * left); 
    }
  }
}
      
	
      




//Calls calculate, sets the stack back to a blank stack
// public calculate() method. Hides private data from user
int TreeCalc::calculate() {
  // call private calculate method here
    int i = calculate(mystack.top());
    return i;
    cleanTree(mystack.top()); 
    while (!(mystack.empty())) {
	mystack.pop();
    }
}
