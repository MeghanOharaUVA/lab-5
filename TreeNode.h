// Meghan O'Hara mmo2bf 10/9/2016 TreeNode.h

// TreeNode.h: TreeNode class definition
// CS 2150: Lab 5


#ifndef TREENODE_H
#define TREENODE_H

#include <string>
using namespace std;

class TreeNode {
public:
  TreeNode();
    TreeNode(const string & val); 

private:
    string value;
    TreeNode *left, *right;  
    friend class TreeCalc;  
};

#endif
